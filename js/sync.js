setTimeout(func, 5000);
function func() {
	google.maps.event.addListener(map1, 'bounds_changed', (function () {
		map2.setCenter(map1.getCenter());
		map2.setZoom(map1.getZoom());
		map3.setCenter(map1.getCenter());
		map3.setZoom(map1.getZoom());
        map4.setCenter(map1.getCenter());
        map4.setZoom(map1.getZoom());
        map5.setCenter(map1.getCenter());
        map5.setZoom(map1.getZoom());
        map6.setCenter(map1.getCenter());
        map6.setZoom(map1.getZoom());
	}));
	google.maps.event.addListener(map2, 'bounds_changed', (function () {
		map1.setCenter(map2.getCenter());
		map1.setZoom(map2.getZoom());
		map3.setCenter(map2.getCenter());
        map3.setZoom(map2.getZoom());
        map4.setCenter(map2.getCenter());
        map4.setZoom(map2.getZoom());
        map5.setCenter(map2.getCenter());
        map5.setZoom(map2.getZoom());
        map6.setCenter(map2.getCenter());
        map6.setZoom(map2.getZoom());
	}));
	google.maps.event.addListener(map3, 'bounds_changed', (function () {
        map1.setCenter(map3.getCenter());
        map1.setZoom(map3.getZoom());
        map2.setCenter(map3.getCenter());
        map2.setZoom(map3.getZoom());
        map4.setCenter(map3.getCenter());
        map4.setZoom(map3.getZoom());
        map5.setCenter(map3.getCenter());
        map5.setZoom(map3.getZoom());
        map6.setCenter(map3.getCenter());
        map6.setZoom(map3.getZoom());
    }));
    google.maps.event.addListener(map4, 'bounds_changed', (function () {
        map1.setCenter(map4.getCenter());
        map1.setZoom(map4.getZoom());
        map2.setCenter(map4.getCenter());
        map2.setZoom(map4.getZoom());
        map3.setCenter(map4.getCenter());
        map3.setZoom(map4.getZoom());
        map5.setCenter(map4.getCenter());
        map5.setZoom(map4.getZoom());
        map6.setCenter(map4.getCenter());
        map6.setZoom(map4.getZoom());
    }));
    google.maps.event.addListener(map5, 'bounds_changed', (function () {
        map1.setCenter(map5.getCenter());
        map1.setZoom(map5.getZoom());
        map2.setCenter(map5.getCenter());
        map2.setZoom(map5.getZoom());
        map3.setCenter(map5.getCenter());
        map3.setZoom(map5.getZoom());
        map4.setCenter(map5.getCenter());
        map4.setZoom(map5.getZoom());
        map6.setCenter(map5.getCenter());
        map6.setZoom(map5.getZoom());
    }));
    google.maps.event.addListener(map6, 'bounds_changed', (function () {
        map1.setCenter(map6.getCenter());
        map1.setZoom(map6.getZoom());
        map2.setCenter(map6.getCenter());
        map2.setZoom(map6.getZoom());
        map3.setCenter(map6.getCenter());
        map3.setZoom(map6.getZoom());
        map4.setCenter(map6.getCenter());
        map4.setZoom(map6.getZoom());
        map5.setCenter(map6.getCenter());
        map5.setZoom(map6.getZoom());
    }));
}