var map1, map2, map3,map4, map5, map6;
function initialize() {
    // Map options: basic configurations
    var mapOptions = {
        //Default zoom
        zoom: 15,
        //Centre to Kelowna
        center: new google.maps.LatLng(49.8873255, -119.4904205),
        //Disable UI
        disableDefaultUI:true
    };

    // Initialize Map objects
    map1 = new google.maps.Map(document.getElementById('map1'),
        mapOptions);
    map2 = new google.maps.Map(document.getElementById('map2'),
        mapOptions);
    map3 = new google.maps.Map(document.getElementById('map3'),
        mapOptions);
    map4 = new google.maps.Map(document.getElementById('map4'),
        mapOptions);
    map5 = new google.maps.Map(document.getElementById('map5'),
        mapOptions);
    map6 = new google.maps.Map(document.getElementById('map6'),
        mapOptions);

    // Style content: google map style format sheet
    // SURFACE: removed poi
    var style1 = [
        {
            "featureType": "poi",
            "stylers": [
                {"visibility": "off"}
            ]
        }
    ];
    // UNDERGROUND: removed poi and added orange hue
    var style2 = [
        {
            "featureType": "poi",
            "stylers": [
                {"visibility": "off"}
            ]
        },{
            "stylers": [
                { "hue": "#ff9900" },
                { "lightness": -10 }
            ]
        }
    ];

    // Style name: string on the button
    var styleName1 = {
        name: 'SURFACE'
    };
    var styleName2 = {
        name: 'UNDERGROUND'
    };

    // Set map type: last type would be loaded by default
    var type2 = new google.maps.StyledMapType(
        style2, styleName2);
    var type1 = new google.maps.StyledMapType(
        style1, styleName1);
    map1.mapTypes.set('SURFACE', type1);
    map1.setMapTypeId('SURFACE');
    map2.mapTypes.set('SURFACE', type1);
    map2.setMapTypeId('SURFACE');
    map3.mapTypes.set('SURFACE', type1);
    map3.setMapTypeId('SURFACE');
    map4.mapTypes.set('UNDERGROUND', type2);
    map4.setMapTypeId('UNDERGROUND');
    map5.mapTypes.set('UNDERGROUND', type2);
    map5.setMapTypeId('UNDERGROUND');
    map6.mapTypes.set('UNDERGROUND', type2);
    map6.setMapTypeId('UNDERGROUND');

    var baseStyle = {
        strokeColor: 'grey',
        strokeWeight: 5
    }
    var overlapStyle = {
        strokeColor:'orange',
        strokeWeight: 2,
        strokeOpacity: 0.5
    }
    var conflctStyle = {
        strokeColor:'red',
        strokeWeight: 3,
        stokeOpacity: 0.5
    }
    map1.data.loadGeoJson('map1_get_json.php')
    map1.data.setStyle(baseStyle);
    map2.data.loadGeoJson('map2_get_json.php');
    map2.data.setStyle(baseStyle);
    map3.data.loadGeoJson('map3_get_json.php');
    map3.data.setStyle(baseStyle);
    map4.data.loadGeoJson('map4_get_json.php');
    map4.data.setStyle(baseStyle);
    map5.data.loadGeoJson('map5_get_json.php');
    map5.data.setStyle(baseStyle);
    map6.data.loadGeoJson('map6_get_json.php');
    map6.data.setStyle(baseStyle);
}
google.maps.event.addDomListener(window, 'load', initialize);
