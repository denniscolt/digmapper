// This java script only works for google map api v2
var side_bar_html = "";
var gmarkers = [];
var htmls = [];
var i = 0;

function createMarker(point,name,html) {
var marker = new GMarker(point);
GEvent.addListener(marker, "click", function() {
  marker.openInfoWindowHtml(html);
});
gmarkers[i] = marker;
htmls[i] = html;
side_bar_html += '<a href="javascript:myclick(' + i + ')">' + name + '<\/a><br>';
i++;
return marker;
}

function myclick(i) {
gmarkers[i].openInfoWindowHtml(htmls[i]);
}

process_it = function(doc) {
var jsonData = eval('(' + doc + ')');

for (var i=0; i<jsonData.markers.length; i++) {
  var marker = createMarker(jsonData.markers[i].point, jsonData.markers[i].label, jsonData.markers[i].html);
  map.addOverlay(marker);
}

document.getElementById("side_bar").innerHTML = side_bar_html;

for (var i=0; i<jsonData.lines.length; i++) {
  map.addOverlay(new GPolyline(jsonData.lines[i].points, jsonData.lines[i].colour, jsonData.lines[i].width)); 
}
}         
// GDownloadUrl is legacy function in google map v2
GDownloadUrl("json/projects.json", process_it);