var map2016;
var kelowna = new google.maps.LatLng(49.8873255,-119.4904205);
function initialize() {

	// Map options
	var mapOptions = {
		zoom: 15,
		center: kelowna,
		mapTypeControlOptions: {
     		mapTypeIds: ['underground','surface']
    	}
	};
	
	map2016 = new google.maps.Map(document.getElementById('map2'),
	mapOptions);

	// Map style configuration
	var undergroundStyles = [
		{
		//This set the theme color to brown
		 	 stylers: [
				{ hue: '#FF7F00' },
				{ visibility: 'simplified' },
				{ gamma: 0.5 },
				{ weight: 0.5 }
		  ]
		},{
      		featureType: 'road.highway',
      		elementType: 'geometry',
      		stylers: [
        		{ hue: '#ff0022' },
       			{ saturation: 60 },
        		{ lightness: -20 }
      		]
    	},{
      		featureType: 'road.arterial',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#2200ff' },
        		{ lightness: -40 },
        		{ visibility: 'simplified' },
        		{ saturation: 30 }
      		]
    	},{
      		featureType: 'road.local',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#f6ff00' },
        		{ saturation: 50 },
        		{ gamma: 0.7 },
       	 		{ visibility: 'simplified' }
      		]
    	},{
      		featureType: 'water',
      		elementType: 'geometry',
      		stylers: [
        	{ saturation: 40 },
        	{ lightness: 40 }
      		]
    	},{
      		featureType: 'road.highway',
      		elementType: 'labels',
      		stylers: [
        		{ visibility: 'on' },
        		{ saturation: 98 }
      		]
    	},{
			featureType: 'administrative.locality',
			elementType: 'labels',
			stylers: [
				{ hue: '#0022ff' },
				{ saturation: 50 },
				{ lightness: -10 },
				{ gamma: 0.90 }
			]
		},{
			featureType: 'transit.line',
			elementType: 'geometry',
			stylers: [
				{ hue: '#ff0000' },
				{ visibility: 'on' },
				{ lightness: -70 }
			]
		}
	];
	
	// Map Style
	var undergroundStyledMapOptions = {
    name: 'Underground'
  	};

  	var undergroundMapType = new google.maps.StyledMapType(
      undergroundStyles, undergroundStyledMapOptions);

  	map2016.mapTypes.set('underground', undergroundMapType);
  	map2016.setMapTypeId('underground');
  	
  	// Map style configuration
	var surfaceStyles = [
		{
			//This set the theme color to blue
		 	 stylers: [
				{ hue: '#0000FF' },
				{ visibility: 'simplified' },
				{ gamma: 0.5 },
				{ weight: 0.5 }
		  ]
		},{
      		featureType: 'road.highway',
      		elementType: 'geometry',
      		stylers: [
        		{ hue: '#ff0022' },
       			{ saturation: 60 },
        		{ lightness: -20 }
      		]
    	},{
      		featureType: 'road.arterial',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#2200ff' },
        		{ lightness: -40 },
        		{ visibility: 'simplified' },
        		{ saturation: 30 }
      		]
    	},{
      		featureType: 'road.local',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#f6ff00' },
        		{ saturation: 50 },
        		{ gamma: 0.7 },
       	 		{ visibility: 'simplified' }
      		]
    	},{
      		featureType: 'water',
      		elementType: 'geometry',
      		stylers: [
        	{ saturation: 40 },
        	{ lightness: 40 }
      		]
    	},{
      		featureType: 'road.highway',
      		elementType: 'labels',
      		stylers: [
        		{ visibility: 'on' },
        		{ saturation: 98 }
      		]
    	},{
			featureType: 'administrative.locality',
			elementType: 'labels',
			stylers: [
				{ hue: '#0022ff' },
				{ saturation: 50 },
				{ lightness: -10 },
				{ gamma: 0.90 }
			]
		},{
			featureType: 'transit.line',
			elementType: 'geometry',
			stylers: [
				{ hue: '#ff0000' },
				{ visibility: 'on' },
				{ lightness: -70 }
			]
		}
	];
	
	// Map Style
	var surfaceStyledMapOptions = {
    name: 'Surface'
  	};

  	var surfaceMapType = new google.maps.StyledMapType(
      surfaceStyles, surfaceStyledMapOptions);

  	map2016.mapTypes.set('surface', surfaceMapType);
  	map2016.setMapTypeId('surface');
  	
  	// Line coords
  	var project1Coordinates = [
    new google.maps.LatLng(49.87823,-119.49020),
    new google.maps.LatLng(49.85825,-119.49020)
  	];
  	var project2Coordinates = [
    new google.maps.LatLng(49.88631,-119.47985),
    new google.maps.LatLng(49.88633,-119.49341)
  	];
  	var project3Coordinates = [
    new google.maps.LatLng(49.86342,-119.47732),
    new google.maps.LatLng(49.82342,-119.48234)
  	];
  	var project4Coordinates = [
    new google.maps.LatLng(49.87234,-119.46213),
    new google.maps.LatLng(49.87393,-119.47341)
  	];
  	
  	// Line object
	var project1Path = new google.maps.Polyline({
    	path: project1Coordinates,
    	geodesic: true,
    	strokeColor: '#FF0000',
    	strokeOpacity: 1.0,
    	strokeWeight: 2
  	});
  	var project2Path = new google.maps.Polyline({
    	path: project2Coordinates,
    	geodesic: true,
    	strokeColor: '#FF0000',
    	strokeOpacity: 1.0,
    	strokeWeight: 2
  	});
  	var project3Path = new google.maps.Polyline({
    	path: project3Coordinates,
    	geodesic: true,
    	strokeColor: '#FF0000',
    	strokeOpacity: 1.0,
    	strokeWeight: 2
  	});
  	var project4Path = new google.maps.Polyline({
    	path: project4Coordinates,
    	geodesic: true,
    	strokeColor: '#FF0000',
    	strokeOpacity: 1.0,
    	strokeWeight: 2
  	});
	// Line render
	  project1Path.setMap(map2016);
	  project2Path.setMap(map2016);
	  project3Path.setMap(map2016);
	  project4Path.setMap(map2016);
}
google.maps.event.addDomListener(window, 'load', initialize);
