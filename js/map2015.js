var map2015;
var infowindow;
var kelowna = new google.maps.LatLng(49.8873255,-119.4904205);
function initialize() {

	// Map options
	var mapOptions = {
		zoom: 15,
		center: kelowna,
		mapTypeControlOptions: {
     		mapTypeIds: ['underground','surface']
    	}
	};
	
	map2015 = new google.maps.Map(document.getElementById('map1'),
	mapOptions);

	// Map style configuration
	var undergroundStyles = [
		{
		//This set the theme color to brown
		 	 stylers: [
				{ hue: '#FF7F00' },
				{ visibility: 'simplified' },
				{ gamma: 0.5 },
				{ weight: 0.5 }
		  ]
		},{
      		featureType: 'road.highway',
      		elementType: 'geometry',
      		stylers: [
        		{ hue: '#ff0022' },
       			{ saturation: 60 },
        		{ lightness: -20 }
      		]
    	},{
      		featureType: 'road.arterial',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#2200ff' },
        		{ lightness: -40 },
        		{ visibility: 'simplified' },
        		{ saturation: 30 }
      		]
    	},{
      		featureType: 'road.local',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#f6ff00' },
        		{ saturation: 50 },
        		{ gamma: 0.7 },
       	 		{ visibility: 'simplified' }
      		]
    	},{
      		featureType: 'water',
      		elementType: 'geometry',
      		stylers: [
        	{ saturation: 40 },
        	{ lightness: 40 }
      		]
    	},{
      		featureType: 'road.highway',
      		elementType: 'labels',
      		stylers: [
        		{ visibility: 'on' },
        		{ saturation: 98 }
      		]
    	},{
			featureType: 'administrative.locality',
			elementType: 'labels',
			stylers: [
				{ hue: '#0022ff' },
				{ saturation: 50 },
				{ lightness: -10 },
				{ gamma: 0.90 }
			]
		},{
			featureType: 'transit.line',
			elementType: 'geometry',
			stylers: [
				{ hue: '#ff0000' },
				{ visibility: 'on' },
				{ lightness: -70 }
			]
		}
	];
	
	// Map Style
	var undergroundStyledMapOptions = {
    name: 'Underground'
  	};

  	var undergroundMapType = new google.maps.StyledMapType(
      undergroundStyles, undergroundStyledMapOptions);

  	map2015.mapTypes.set('underground', undergroundMapType);
  	map2015.setMapTypeId('underground');
  	
  	// Map style configuration
	var surfaceStyles = [
		{
			//This set the theme color to blue
		 	 stylers: [
				{ hue: '#0000FF' },
				{ visibility: 'simplified' },
				{ gamma: 0.5 },
				{ weight: 0.5 }
		  ]
		},{
      		featureType: 'road.highway',
      		elementType: 'geometry',
      		stylers: [
        		{ hue: '#ff0022' },
       			{ saturation: 60 },
        		{ lightness: -20 }
      		]
    	},{
      		featureType: 'road.arterial',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#2200ff' },
        		{ lightness: -40 },
        		{ visibility: 'simplified' },
        		{ saturation: 30 }
      		]
    	},{
      		featureType: 'road.local',
      		elementType: 'all',
      		stylers: [
        		{ hue: '#f6ff00' },
        		{ saturation: 50 },
        		{ gamma: 0.7 },
       	 		{ visibility: 'simplified' }
      		]
    	},{
      		featureType: 'water',
      		elementType: 'geometry',
      		stylers: [
        	{ saturation: 40 },
        	{ lightness: 40 }
      		]
    	},{
      		featureType: 'road.highway',
      		elementType: 'labels',
      		stylers: [
        		{ visibility: 'on' },
        		{ saturation: 98 }
      		]
    	},{
			featureType: 'administrative.locality',
			elementType: 'labels',
			stylers: [
				{ hue: '#0022ff' },
				{ saturation: 50 },
				{ lightness: -10 },
				{ gamma: 0.90 }
			]
		},{
			featureType: 'transit.line',
			elementType: 'geometry',
			stylers: [
				{ hue: '#ff0000' },
				{ visibility: 'on' },
				{ lightness: -70 }
			]
		}
	];
	
	// Map Style
	var surfaceStyledMapOptions = {
    name: 'Surface'
  	};

  	var surfaceMapType = new google.maps.StyledMapType(
      surfaceStyles, surfaceStyledMapOptions);

  	map2015.mapTypes.set('surface', surfaceMapType);
  	map2015.setMapTypeId('surface');

    map2015.data.loadGeoJson('test.php');
}
google.maps.event.addDomListener(window, 'load', initialize);
