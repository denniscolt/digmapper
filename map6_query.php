<?php
/**
 * Title:   MySQL to GeoJSON (Requires https://github.com/phayes/geoPHP)
 * Notes:   Query a MySQL table or view and return the results in GeoJSON format, suitable for use in OpenLayers, Leaflet, etc.
 * Author:  Bryan R. McBride, GISP
 * Contact: bryanmcbride.com
 * GitHub:  https://github.com/bmcbride/PHP-Database-GeoJSON
 */
# Include required geoPHP library and define wkb_to_json function
include_once('geoPHP/geoPHP.inc');
function wkb_to_json($wkb) {
    $geom = geoPHP::load($wkb,'wkb');
    return $geom->out('json');
}
# Connect to MySQL database
$conn = new PDO('mysql:host=localhost;dbname=digmapper','root','');
# Build SQL SELECT statement and return the geometry as a WKB element
$clr = 'delete from map6inter';
$cmd = 'insert into map61inter SELECT a.id, a.line,a.name FROM map6 As a, map6 As b WHERE MBRIntersects(a.line,b.line)=1 AND a.id<>b.id';
$sql = 'SELECT AsWKB(line) AS wkb FROM map6inter';
# Try query or error
$conn->query($clr);
$conn->query($cmd);
$rs = $conn->query($sql);
if (!$rs) {
    echo 'An SQL error occured.\n';
    exit;
}
# Build GeoJSON feature collection array
$geojson = array(
    'type'      => 'FeatureCollection',
    'features'  => array()
);
# Loop through rows to build feature arrays
while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
    $properties = $row;
    # Remove wkb and geometry fields from properties
    unset($properties['wkb']);
    unset($properties['SHAPE']);
    $feature = array(
        'type' => 'Feature',
        'geometry' => json_decode(wkb_to_json($row['wkb'])),
        'properties' => $properties
    );
    # Add feature arrays to feature collection array
    array_push($geojson['features'], $feature);
}
header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
$conn = NULL;
?>